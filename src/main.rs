extern crate ini;
use ini::Ini;
use std::io;
use std::io::Read;
use std::io::Write;
use std::path::Path;
use std::process;

fn clear() {
    print!("{esc}[2J{esc}[1;1H", esc = 27 as char);
}

fn pause() {
    let mut stdin = io::stdin();
    let mut stdout = io::stdout();

    // Print without a newline and flush manually.
    write!(stdout, "\nPress any key to continue...").unwrap();
    stdout.flush().unwrap();

    // Read a single byte and discard
    let _ = stdin.read(&mut [0u8]).unwrap();
}

fn init() {
    if !Path::new("config.ini").exists() {
        let mut new_config = Ini::new();
        new_config
            .with_section(Some("Finder"))
            .set("frequency", "8")
            .set("frequency_unit", "MHz")
            .set("delay", "10")
            .set("delay_unit", "ms")
            .set("num_cycles", "two")
            .set("output_mode", "first");
        new_config.write_to_file("config.ini").unwrap();
    }
}

struct Config {
    frequency: String,
    frequency_unit: String,
    delay: String,
    delay_unit: String,
    num_cycles: String,
    output_mode: String,
}

static mut CONFIG: Config = Config {
    frequency: String::new(),
    frequency_unit: String::new(),
    delay: String::new(),
    delay_unit: String::new(),
    num_cycles: String::new(),
    output_mode: String::new(),
};

unsafe fn update_conf() {
    let from_config = Ini::load_from_file("config.ini").unwrap();
    let section = from_config.section(Some("Finder")).unwrap();

    CONFIG.frequency = section.get("frequency").unwrap().to_string();
    CONFIG.frequency_unit = section.get("frequency_unit").unwrap().to_string();
    CONFIG.delay = section.get("delay").unwrap().to_string();
    CONFIG.delay_unit = section.get("delay_unit").unwrap().to_string();
    CONFIG.num_cycles = section.get("num_cycles").unwrap().to_string();
    CONFIG.output_mode = section.get("output_mode").unwrap().to_string();
}

unsafe fn save_conf() {
    let mut from_config = Ini::load_from_file("config.ini").unwrap();
    from_config
        .with_section(Some("Finder"))
        .set("frequency", CONFIG.frequency.to_string())
        .set("frequency_unit", CONFIG.frequency_unit.to_string())
        .set("delay", CONFIG.delay.to_string())
        .set("delay_unit", CONFIG.delay_unit.to_string())
        .set("num_cycles", CONFIG.num_cycles.to_string())
        .set("output_mode", CONFIG.output_mode.to_string());
    from_config.write_to_file("config.ini").unwrap();
}

fn main() {
    init();

    let mut command: i32 = 0;
    loop {
        match command {
            0 => command = main_menu(),
            1 => command = unsafe { find() },
            2 => command = unsafe { settings_menu() },
            3 => process::exit(0),
            _ => command = main_menu(),
        }
    }
}

fn main_menu() -> i32 {
    let mut input = String::new();
    loop {
        clear();
        println!("1 > Start");
        println!("2 > Configuration");
        println!("3 > Exit");

        print!("\n>> ");
        io::stdout().flush().unwrap();
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");

        if input.trim() == "1" || input.trim() == "2" || input.trim() == "3" {
            let command: i32 = input.trim().parse().unwrap();
            return command;
        }
    }
}

unsafe fn settings_menu() -> i32 {
    let mut input = String::new();
    update_conf();
    clear();

    print!("1 > MCU frequency: ");
    println!("{} {}\n", CONFIG.frequency, CONFIG.frequency_unit);
    print!("2 > Delay: ");
    println!("{} {}\n", CONFIG.delay, CONFIG.delay_unit);
    println!("3 > Number of cycles:");
    println!("\t[@] Auto");
    if CONFIG.num_cycles == "two" {
        println!("\t[x] Two");
    } else {
        println!("\t[ ] Two");
    }
    if CONFIG.num_cycles == "three" {
        println!("\t[x] Three\n");
    } else {
        println!("\t[ ] Three\n");
    }
    println!("4 > Output mode:");
    if CONFIG.output_mode == "first" {
        println!("\t[x] First relevant");
    } else {
        println!("\t[ ] First relevant");
    }
    println!("\t[@] Paged");
    if CONFIG.output_mode == "all" {
        println!("\t[x] All results\n");
    } else {
        println!("\t[ ] All results\n");
    }
    println!("5 > Back to main menu\n");

    print!(">> ");
    io::stdout().flush().unwrap();
    io::stdin()
        .read_line(&mut input)
        .expect("Failed to read line");

    if input.trim() == "1" {
        let mut new = String::new();
        print!("\nEnter MCU frequency (MHz): ");
        io::stdout().flush().unwrap();
        io::stdin()
            .read_line(&mut new)
            .expect("Failed to read line");
        CONFIG.frequency = new.trim().to_string();
        save_conf();
    } else if input.trim() == "2" {
        let mut new = String::new();
        print!("\nEnter delay (ms): ");
        io::stdout().flush().unwrap();
        io::stdin()
            .read_line(&mut new)
            .expect("Failed to read line");
        CONFIG.delay = new.trim().to_string();
        save_conf();
    } else if input.trim() == "3" {
        if CONFIG.num_cycles == "two" {
            CONFIG.num_cycles = "three".to_string()
        } else {
            CONFIG.num_cycles = "two".to_string()
        }
        save_conf();
    } else if input.trim() == "4" {
        if CONFIG.output_mode == "first" {
            CONFIG.output_mode = "all".to_string()
        } else {
            CONFIG.output_mode = "first".to_string()
        }
        save_conf();
    } else if input.trim() == "5" {
        return 0;
    }

    return 2;
}

unsafe fn find() -> i32 {
    update_conf();
    clear();

    let mut i1 = 4;
    let mut i2 = 4;
    let mut i3 = 4;
    let mut r17 = 1;
    let mut r18 = 1;
    let mut r19 = 1;
    let mut j = 0;

    let frequency_int: i32 = CONFIG.frequency.trim().parse().unwrap();
    let delay_f64: f64 = CONFIG.delay.trim().parse().unwrap();

    let k = (frequency_int as f64) * 1000000.0 * delay_f64 / 1000.0;
    println!(
        "Your configuration: {} {}, {} {}.",
        CONFIG.frequency, CONFIG.frequency_unit, CONFIG.delay, CONFIG.delay_unit
    );
    println!(
        "k = {} {} * {} {} = {} Hz * {} s = {}\n",
        CONFIG.frequency,
        CONFIG.frequency_unit,
        CONFIG.delay,
        CONFIG.delay_unit,
        frequency_int * 1000000,
        delay_f64 / 1000.0,
        k
    );

    while r19 < 256 {
        if CONFIG.num_cycles == "two" {
            if (i1 * r17 + i2) * r18 == (k as i32) {
                j += 1;
                println!(
                    "{}. i1 = {}, r17 = {}, i2 = {}, r18 = {};",
                    j, i1, r17, i2, r18
                );

                if CONFIG.output_mode == "first" {
                    break;
                }
            }
        } else {
            if ((i1 * r17 + i2) * r18 + i3) * r19 == (k as i32) {
                j += 1;
                println!(
                    "{}. i1 = {}, r17 = {}, i2 = {}, r18 = {}, i3 = {}, r19 = {};",
                    j, i1, r17, i2, r18, i3, r19
                );

                if CONFIG.output_mode == "first" {
                    break;
                }
            }
        }

        i1 += 1;
        if i1 > 14 {
            i2 += 1;
            i1 = 4;
        }
        if i2 > 14 {
            if CONFIG.num_cycles == "two" {
                r17 += 1;
            } else {
                i3 += 1;
            }
            i2 = 4;
        }
        if CONFIG.num_cycles != "two" && i3 > 14 {
            r17 += 1;
            i3 = 4;
        }
        if r17 > 255 {
            r18 += 1;
            r17 = 1;
        }
        if r18 > 255 {
            if CONFIG.num_cycles == "two" {
                break;
            } else {
                r19 += 1;
                r18 = 1;
            }
        }
    }

    if j == 0 {
        println!("Relevant parameters not found.");
    }

    pause();
    return 0;
}
