# AVR delay

Other languages: [Русский](https://github.com/vladaris/AVR-delay/blob/main/documentation/ru/README.md)

## How to use

## How it works

## Features

### Delay types
- [X] Delay by subroutine
- [ ] Timer-counters delay

### Additional features
- [ ] Generate assembler code

### Settings
- [X] Configure MCU frequency
- [X] Configure delay
	- [ ] s
	- [X] ms
	- [ ] µs
- [X] Configure number of cycles (subroutine)
	- [ ] Auto
	- [X] Two
	- [X] Three
- [X] Configure output mode
	- [X] First relevant
	- [ ] Paged
	- [X] All results